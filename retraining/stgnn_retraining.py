# Copyright (c) 2024 Intracom Telecom (ICOM)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Maria Eleftheria Vlontzou - Author


import time
import requests
import pandas as pd
import numpy as np
import json
import requests
from kubernetes import client, config
import os
import warnings
import kfp
from kfp import Client

import logging


warnings.filterwarnings("ignore")
#evaluate mape
#store values for retraining

forecast_horizons = [5,15]
retraining_flags = {'cpu_5': False, 'cpu_15': False, 'ram_5': False, 'ram_15': False}

def get_input_length(forecast_horizon):
  if (forecast_horizon == 5 or forecast_horizon == 15):
    freq = 1
    input_length = forecast_horizon / freq
  elif (forecast_horizon == 30):
    freq = 5
    input_length = forecast_horizon / freq

  return input_length, freq

#forecasts_path  =  "../../../CODECO/gnn_models/forecasts5.csv"
data_path = os.environ.get('GNN_DATA_PATH')
forecasts_subpath = os.environ.get('GNN_FORECASTS_PATH')
#data_path = "../../../CODECO/synthetic-data-generator/data-extractor/data.csv"


def read_forecasts(forecasts_path):

  forecasted_vals = pd.read_csv(forecasts_path, header=0)
  node_names = forecasted_vals['node_name'].unique()
  node_names_num = forecasted_vals['node_name'].nunique()

  return forecasted_vals, node_names_num

def read_actual_values():
  input_vals = pd.read_csv(data_path, header = 0)
  input_data = input_vals[['time', 'node_name', 'cpu', 'mem']]

  return input_data

def merge_df(actual, forecasted):
  actual['time'] = pd.to_datetime(actual['time'])
  forecasted['forecast_timestamp'] = pd.to_datetime(forecasted['forecast_timestamp'])

  actual.loc[:,'common_time'] = actual['time'].dt.strftime('%Y-%m-%d %H:%M')
  forecasted.loc[:,'common_time'] = forecasted['forecast_timestamp'].dt.strftime('%Y-%m-%d %H:%M')

  merged_df = pd.merge(actual, forecasted, on=['common_time','node_name'])

  return merged_df

def evaluate_mape(merged_df):
  merged_df['abs_diff_cpu'] = abs(merged_df['cpu'] - merged_df['cpu_prediction'])
  merged_df['abs_percentage_error_cpu'] = (merged_df['abs_diff_cpu'] / merged_df['cpu']) * 100
  mape_cpu = merged_df['abs_percentage_error_cpu'].mean()

  merged_df['abs_diff_ram'] = abs(merged_df['mem'] - merged_df['ram_prediction'])
  merged_df['abs_percentage_error_ram'] = (merged_df['abs_diff_ram'] / merged_df['mem']) * 100
  mape_ram = merged_df['abs_percentage_error_ram'].mean()

  print("cpu", mape_cpu)
  print("ram", mape_ram)

  return mape_cpu, mape_ram

def trigger_retraining(metric, forecast_horizon, node_num, mape):

  #Get service ip and port
  config.load_incluster_config()
  namespace =  os.environ.get('NAMESPACE')
  service_name = 'ml-pipeline'
  v1 = client.CoreV1Api()
  service = v1.read_namespaced_service(service_name, namespace)
  cluster_ip = service.spec.cluster_ip
  port = service.spec.ports[0].port
  print(cluster_ip)
  print(port)
  pipeline_id = '7f627cfd-dd32-4af4-93e1-365431481939'  # ID of your pipeline
  experiment_name = 'gnns_first'  # The experiment you want to use or create
  # experiment = client.create_experiment(experiment_name)
  model_subpath = os.environ.get('MODELS_PATH')
  model_path = f'/mnt/output/{model_subpath}trained_models_cpu_{node_num}_{forecast_horizon}m/'
  # Define pipeline parameters if needed
  data_url = f'/mnt/output/{data_path}'
  topology = os.environ.get('TOPOLOGY_PATH')
  topology_path = f'/mnt/output/{topology}'
  pipeline_params = {
      'data_url': data_url,
      'forecast_horizon': forecast_horizon,
      'metric': metric,
      'topology_path':  topology_path,
      'model_path': model_path,
      'prev_mape': mape

  }

  credentials = kfp.auth.ServiceAccountTokenVolumeCredentials(path=None)
  kfp_client = kfp.Client(host=f"http://{cluster_ip}:8888", credentials=credentials)

  #client.create_experiment(name=gnns_test)
  #print(client.get_experiment(experiment_name=gnns_test))
  print(kfp_client.run_pipeline(experiment_id='0257b5ba-7dbd-486e-b438-0dd06a0fd450', job_name='gnn_retraining', params=pipeline_params, pipeline_id=pipeline_id))
   # Headers for the POST request

'''
  try:
    experiment = client.create_experiment(name=experiment_name)
  except ApiException as e:
    print(f"Failed to create experiment: {e}")

  # List experiments to check if the experiment already exists
  experiments = client.list_experiments()
  print("Experiments:", experiments)


  run_name = 'My Pipeline Run'

  try:
    run = client.run_pipeline(
        experiment_id=experiment.id,
        job_name=run_name,
        params=pipeline_params,
        pipeline_id=pipeline_id
    )
    print("Run created successfully:", run.id)
  except ApiException as e:
    print(f"Failed to create run: {e}")
    '''

instances = 5


for forecast_horizon in forecast_horizons:
  input_sequence_length, freq = get_input_length(forecast_horizon)
  forecasts_path = f"{forecasts_subpath}/forecasts{forecast_horizon}.csv"
  forecasted_vals, node_num = read_forecasts(forecasts_path)
  num_rows_act = int(instances * node_num)
  num_rows_act2 = int((instances+2) * node_num)
  num_rows_for = int(node_num * input_sequence_length)
  num_rows_dif  = num_rows_act + num_rows_for
  actual_vals = read_actual_values()

  forecasts = forecasted_vals[-num_rows_dif:-num_rows_for]
  actual =  actual_vals[-num_rows_act2:]

  comparison_dataset = merge_df(actual, forecasts)
  #print(comparison_dataset)
  mape_cpu, mape_ram = evaluate_mape(comparison_dataset)
  #print("forecasts", forecast_horizon)
  #print(forecasts)
  #print("actual", forecast_horizon)
  #print(actual)
  if (mape_cpu > 0.8):
     trigger_retraining(metric = 'cpu', forecast_horizon = forecast_horizon, node_num = node_num, mape = mape_cpu)
  elif (mape_ram > 0.8):
     trigger_retraining(metric = 'ram', forecast_horizon = forecast_horizon, node_num = node_num, mape = mape_ram)
