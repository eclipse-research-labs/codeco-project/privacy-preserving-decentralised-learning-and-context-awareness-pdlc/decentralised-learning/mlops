# Copyright (c) 2024 Intracom Telecom (ICOM)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Maria Eleftheria Vlontzou - Author


import argparse
from pathlib import Path
import numpy as np
import pandas as pd

def stgnn_preprocessing(data_url, metric, forecast_horizon, output_path1, output_path2, output_path3, output_path4):

    def load_and_transform(data_url):
        data = pd.read_csv(data_url, low_memory=False)
        node_data_selected = data[['timestamp', 'node_name', 'normalized_cpu_values', 'normalized_memory_values']]

        data_selected = node_data_selected

        selected_nodes_2 = []
        node_names = node_data_selected['node_name'].unique()

        for i in range(len(node_names)): #creating a list of dataframes, one dataframe for each node
            new_nodes = data_selected[data_selected['node_name'] == node_names[i]]
            new_nodes.reset_index(drop=True, inplace=True)
            selected_nodes_2.append(new_nodes)

        return selected_nodes_2

    #function to produce sampled metrics per 5 minutes out of the averaged values per minute
    def five_min_sample(nodes_list):
        nodes_five_min = []
        for node in nodes_list:
            res = node[::5]
            nodes_five_min.append(res)
            
        return nodes_five_min


    def create_metrics_arrays(data_path, forecast_horizon):

        forecast_horizon = int(forecast_horizon)
        nodes_list = load_and_transform(data_path).copy()
        #min_mean = minute_mean('cpu', 'mem', nodes_list)
        if (forecast_horizon == 30):
            sampled_data = five_min_sample(nodes_list)  #returns the sampled data per 5 minutes
            dataframes = sampled_data
        else: 
            dataframes = nodes_list

        df2 = pd.concat(dataframes)
        df2.sort_values(by=['timestamp', 'node_name'], inplace=True)
        df2.reset_index(drop=True, inplace=True)

        #separate nodes' dataframes after they have been sampled
        data_selected = df2
        selected_nodes_2 = []
        node_names = df2['node_name'].unique()

        for i in range(len(node_names)):
            new_nodes = data_selected[data_selected['node_name'] == node_names[i]]
            new_nodes.reset_index(drop=True, inplace=True)
            selected_nodes_2.append(new_nodes)

        #create separate array for each metric (cpy and memory)
        cpu_array = []
        ram_array = []
        for i in range(len(selected_nodes_2)):
            cpu_array.append(selected_nodes_2[i]['normalized_cpu_values'].to_numpy())
            ram_array.append(selected_nodes_2[i]['normalized_memory_values'].to_numpy())

        #transform into desired format nd.array of shape (number_of_instances, number_of_nodes)
        cpu_array = np.array(cpu_array)
        transpose_sepeds = np.transpose(cpu_array)
        cpu_array = transpose_sepeds

        ram_array = np.array(ram_array)
        transpose_sepeds = np.transpose(ram_array)
        ram_array = transpose_sepeds

        return cpu_array, ram_array, node_names

    def split_data(data_array: np.ndarray, train_size: float , val_size: float, metric: str):
        """Splits data into train/val/test sets.

        Args:
            data_array: ndarray of shape `(num_time_steps, num_routes)`
            train_size: A float value between 0.0 and 1.0 that represent the proportion of the dataset
                to include in the train split.
            val_size: A float value between 0.0 and 1.0 that represent the proportion of the dataset
                to include in the validation split.

        Returns:
            `train_array`, `val_array`
        """

        num_time_steps = data_array.shape[0]
        num_train, num_val = (
            int(num_time_steps * train_size),
            int(num_time_steps * val_size),
        )
        num_train = int(num_time_steps * train_size)
        
        train_array = data_array[:num_train]

        val_array = data_array[num_train : (num_train + num_val)]

        test_array = data_array[(num_train + num_val) :]

        return train_array, val_array, test_array


    #Train, validation, test set split
    cpu_arr, ram_arr, node_names = create_metrics_arrays(data_url, forecast_horizon)

    if (metric == 'ram'):
        metric_array = ram_arr
    elif (metric == 'cpu'):
        metric_array = cpu_arr

    #train_size = args.train_size
    #valid_size = args.val_size
    train_size = 0.6
    valid_size = 0.2
    train_array, val_array, test_array = split_data(metric_array, train_size, valid_size, metric)
    train_array_shape = train_array.shape
    val_array_shape = val_array.shape
    test_array_shape = test_array.shape

    Path(args.output_path1).parent.mkdir(parents=True, exist_ok=True)
    Path(args.output_path2).parent.mkdir(parents=True, exist_ok=True)
    Path(args.output_path3).parent.mkdir(parents=True, exist_ok=True)
    Path(args.output_path4).parent.mkdir(parents=True, exist_ok=True)


    train_string = str(train_array.tolist())
    val_string = str(val_array.tolist())
    test_string = str(test_array.tolist())
    forecast_horizon = str(forecast_horizon)
    train_array_shape = str(train_array_shape)
    val_array_shape = str(val_array_shape)
    test_array_shape = str(test_array_shape)
    #mean_string = str(mean.tolist())
    #std_string = str(std.tolist())
    node_names_string = str(node_names.tolist())

    with open(output_path1, 'w+') as f:
        arg1 = train_string
        f.write(arg1)

    with open(output_path2, 'w+') as f:
        arg2= val_string
        f.write(arg2)

    with open(output_path3, 'w+') as f:
        arg3= test_string
        f.write(arg3)

    lines = []
    lines.append(metric + '\n')
    lines.append(forecast_horizon + '\n')
    lines.append(node_names_string + '\n')
    lines.append(train_array_shape + '\n')
    lines.append(val_array_shape + '\n')
    lines.append(test_array_shape + '\n')
    with open(output_path4, 'w+') as f:
        f.writelines(lines)
 
parser = argparse.ArgumentParser(description='command line arguments.')
parser.add_argument('--data_url', type=str, required=True)
parser.add_argument('--metric', type=str, required=True)
parser.add_argument('--forecast_horizon', type=str, required=True)
parser.add_argument('--output_path1', type=str)
parser.add_argument('--output_path2', type=str)
parser.add_argument('--output_path3', type=str)
parser.add_argument('--output_path4', type=str)
args = parser.parse_args()

data_url = args.data_url

if __name__ == '__main__':
	stgnn_preprocessing(args.data_url, args.metric, args.forecast_horizon, args.output_path1, args.output_path2, args.output_path3, args.output_path4)

