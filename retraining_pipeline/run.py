# Copyright (c) 2024 Intracom Telecom (ICOM)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Maria Eleftheria Vlontzou - Author


import kfp
import kfp.components as comp
from kfp import dsl

client = kfp.Client()
# Create factory functions
data_formatting_op = comp.load_component_from_file('data_formatting/data_formatting.yml')
stgnn_tuning_op = comp.load_component_from_file('hyperparameter_tuning/hyperparameter_tuning.yml')
stgnn_lstm_training_op = comp.load_component_from_file('training/training.yml')


# Create pipeline
@dsl.pipeline()
def stgnn_models(
    data_url: dsl.PipelineParam,
    metric: dsl.PipelineParam,
    forecast_horizon: dsl.PipelineParam,
    model_path: dsl.PipelineParam,
    prev_mape: dsl.PipelineParam,
    topology_path: dsl.PipelineParam
):

    r1 = data_formatting_op(data_url = data_url, metric = metric, forecast_horizon = forecast_horizon).add_pvolumes({"/mnt/output": dsl.PipelineVolume(pvc="my-hostpath-pvc")})
    r2 = stgnn_tuning_op(r1.outputs['output_path1'], r1.outputs['output_path2'], r1.outputs['output_path3'], r1.outputs['output_path4'], topology_path = topology_path).add_pvolumes({"/mnt/output": dsl.PipelineVolume(pvc="my-hostpath-pvc")})
    r3 = stgnn_lstm_training_op(r1.outputs['output_path1'], r1.outputs['output_path2'], r1.outputs['output_path3'], r1.outputs['output_path4'], r2.outputs['best_hyperparameters_path'], model_path = model_path, prev_mape = prev_mape, topology_path=topology_path).add_pvolumes({"/mnt/output": dsl.PipelineVolume(pvc="my-hostpath-pvc")})

    
r = kfp.compiler.Compiler().compile(stgnn_models, 'stgnn_retraining.yaml')
