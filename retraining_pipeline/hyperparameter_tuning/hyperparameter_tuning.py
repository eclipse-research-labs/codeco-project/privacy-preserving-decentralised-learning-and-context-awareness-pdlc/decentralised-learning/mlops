# Copyright (c) 2024 Intracom Telecom (ICOM)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Maria Eleftheria Vlontzou - Author


import argparse
from pathlib import Path

import ast
import tensorflow as tf
import numpy as np
import os 
import json
import typing
from tensorflow import keras
from keras import layers
from keras.preprocessing import timeseries_dataset_from_array


def hyperparameter_tuning(input_path1, input_path2, input_path3, input_path4, best_hyperparameters_path, topology_path):#, output_path, labels_output, preds_output):


    def create_tf_dataset(
            data_array: np.ndarray,
            input_sequence_length: int,
            forecast_horizon: int,
            batch_size: int = 128,
            shuffle=True,
            multi_horizon=False,
        ):
            """Creates tensorflow dataset from numpy array.

            This function creates a dataset where each element is a tuple `(inputs, targets)`.
            `inputs` is a Tensor
            of shape `(batch_size, input_sequence_length, num_routes, 1)` containing
            the `input_sequence_length` past values of the timeseries for each node.
            `targets` is a Tensor of shape `(batch_size, forecast_horizon, num_routes)`
            containing the `forecast_horizon`
            future values of the timeseries for each node.

            Args:
                data_array: np.ndarray with shape `(num_time_steps, num_routes)`
                input_sequence_length: Length of the input sequence (in number of timesteps).
                forecast_horizon: If `multi_horizon=True`, the target will be the values of the timeseries for 1 to
                    `forecast_horizon` timesteps ahead. If `multi_horizon=False`, the target will be the value of the
                    timeseries `forecast_horizon` steps ahead (only one value).
                batch_size: Number of timeseries samples in each batch.
                shuffle: Whether to shuffle output samples, or instead draw them in chronological order.
                multi_horizon: See `forecast_horizon`.

            Returns:
                A tf.data.Dataset instance.
            """

            inputs = timeseries_dataset_from_array(
                np.expand_dims(data_array[:-forecast_horizon], axis=-1),
                None,
                sequence_length=input_sequence_length,
                shuffle=False,
                batch_size=batch_size,
            )

            target_offset = (
                input_sequence_length
                if multi_horizon
                else input_sequence_length + forecast_horizon - 1
            )
            target_seq_length = forecast_horizon if multi_horizon else 1
            targets = timeseries_dataset_from_array(
                data_array[target_offset:],
                None,
                sequence_length=target_seq_length,
                shuffle=False,
                batch_size=batch_size,
            )

            dataset = tf.data.Dataset.zip((inputs, targets))
            if shuffle:
                dataset = dataset.shuffle(100)

            return dataset.prefetch(16).cache()


    class GraphInfo:
        def __init__(self, edges: typing.Tuple[list, list], num_nodes: int):
            self.edges = edges
            self.num_nodes = num_nodes



    class GraphConv(layers.Layer):
        def __init__(
            self,
            in_feat,
            out_feat,
            graph_info: GraphInfo,
            aggregation_type="mean",
            combination_type="concat",
            activation: typing.Optional[str] = None,
            **kwargs,
        ):
            super().__init__(**kwargs)
            self.in_feat = in_feat
            self.out_feat = out_feat
            self.graph_info = graph_info
            self.aggregation_type = aggregation_type
            self.combination_type = combination_type
            self.weight = tf.Variable(
                initial_value=keras.initializers.glorot_uniform()(
                    shape=(in_feat, out_feat), dtype="float32"
                ),
                trainable=True,
            )
            self.activation = layers.Activation(activation)

        def aggregate(self, neighbour_representations: tf.Tensor):
            aggregation_func = {
                "sum": tf.math.unsorted_segment_sum,
                "mean": tf.math.unsorted_segment_mean,
                "max": tf.math.unsorted_segment_max,
            }.get(self.aggregation_type)

            if aggregation_func:
                return aggregation_func(
                    neighbour_representations,
                    self.graph_info.edges[0],
                    num_segments=self.graph_info.num_nodes,
                )

            raise ValueError(f"Invalid aggregation type: {self.aggregation_type}")

        def compute_nodes_representation(self, features: tf.Tensor):
            """Computes each node's representation.

            The nodes' representations are obtained by multiplying the features tensor with
            `self.weight`. Note that
            `self.weight` has shape `(in_feat, out_feat)`.

            Args:
                features: Tensor of shape `(num_nodes, batch_size, input_seq_len, in_feat)`

            Returns:
                A tensor of shape `(num_nodes, batch_size, input_seq_len, out_feat)`
            """
            return tf.matmul(features, self.weight)

        def compute_aggregated_messages(self, features: tf.Tensor):
            neighbour_representations = tf.gather(features, self.graph_info.edges[1])
            aggregated_messages = self.aggregate(neighbour_representations)
            return tf.matmul(aggregated_messages, self.weight)

        def update(self, nodes_representation: tf.Tensor, aggregated_messages: tf.Tensor):
            if self.combination_type == "concat":
                h = tf.concat([nodes_representation, aggregated_messages], axis=-1)
            elif self.combination_type == "add":
                h = nodes_representation + aggregated_messages
            else:
                raise ValueError(f"Invalid combination type: {self.combination_type}.")

            return self.activation(h)

        def call(self, features: tf.Tensor):
            """Forward pass.

            Args:
                features: tensor of shape `(num_nodes, batch_size, input_seq_len, in_feat)`

            Returns:
                A tensor of shape `(num_nodes, batch_size, input_seq_len, out_feat)`
            """
            nodes_representation = self.compute_nodes_representation(features)
            aggregated_messages = self.compute_aggregated_messages(features)
            return self.update(nodes_representation, aggregated_messages)
        


    class LSTMGC(layers.Layer):
        """Layer comprising a convolution layer followed by LSTM and dense layers."""

        def __init__(
            self,
            in_feat,
            out_feat,
            lstm_units: int,
            input_seq_len: int,
            output_seq_len: int,
            
            graph_info: GraphInfo,
            graph_conv_params: typing.Optional[dict] = None,
            **kwargs,
        ):
            super().__init__(**kwargs)

            # graph conv layer
            if graph_conv_params is None:
                graph_conv_params = {
                    "aggregation_type": "mean",
                    "combination_type": "concat",
                    "activation": None,
                }
            self.graph_conv = GraphConv(in_feat, out_feat, graph_info, **graph_conv_params)

            self.lstm = layers.LSTM(lstm_units, activation="relu")
            self.dense = layers.Dense(output_seq_len)

            self.input_seq_len, self.output_seq_len = input_seq_len, output_seq_len

        def call(self, inputs):
            """Forward pass.

            Args:
                inputs: tf.Tensor of shape `(batch_size, input_seq_len, num_nodes, in_feat)`

            Returns:
                A tensor of shape `(batch_size, output_seq_len, num_nodes)`.
            """

            # convert shape to  (num_nodes, batch_size, input_seq_len, in_feat)
            inputs = tf.transpose(inputs, [2, 0, 1, 3])

            gcn_out = self.graph_conv(
                inputs
            )  # gcn_out has shape: (num_nodes, batch_size, input_seq_len, out_feat)
            shape = tf.shape(gcn_out)
            num_nodes, batch_size, input_seq_len, out_feat = (
                shape[0],
                shape[1],
                shape[2],
                shape[3],
            )

            # LSTM takes only 3D tensors as input
            gcn_out = tf.reshape(gcn_out, (batch_size * num_nodes, input_seq_len, out_feat))
            lstm_out = self.lstm(
                gcn_out
            )  # lstm_out has shape: (batch_size * num_nodes, lstm_units)

            dense_output = self.dense(
                lstm_out
            )  # dense_output has shape: (batch_size * num_nodes, output_seq_len)
            output = tf.reshape(dense_output, (num_nodes, batch_size, self.output_seq_len))
            return tf.transpose(
                output, [1, 2, 0]
            )  # returns Tensor of shape (batch_size, output_seq_len, num_nodes)
        
    def predict(test_dataset, test_array, model):
        x_test, y = next(test_dataset.as_numpy_iterator())
        y_pred = model.predict(x_test)

        return y, y_pred

    def evaluate_stgnn(y, y_pred):

        stgnn_model_mape = np.abs((y[:, 0, :] - y_pred[:, 0, :]) / y[:, 0, :]).mean()
        print(stgnn_model_mape)
        return stgnn_model_mape


    def best_hyperparameters(hyperparameters_score_list):
        sorted_hyperparameters_list = sorted(hyperparameters_score_list, key=lambda x: x[4], reverse=True)
        best_hyperparameters = sorted_hyperparameters_list[0]
        return best_hyperparameters


    def read_topology(topology):

      # Opening JSON file
      f = open(topology)

      # returns JSON object as a dictionary
      data = json.load(f)
      #data = topology

      # Iterating through the json list
      node_names = []
      for i in data['node_names']:
        node_names.append(np.array(i))
      node_names_array = np.array(node_names)

      connections = []
      for i in data['connections']:
        connections.append(np.array(i))
      adjacency_matrix = np.array(connections)

      return node_names_array, adjacency_matrix

    Path(args.input_path1).parent.mkdir(parents=True, exist_ok=True)
    Path(args.input_path2).parent.mkdir(parents=True, exist_ok=True)
    Path(args.input_path3).parent.mkdir(parents=True, exist_ok=True)
    Path(args.input_path4).parent.mkdir(parents=True, exist_ok=True)
    
    with open(input_path4, 'r') as reader:
        lines = reader.readlines()
        metric = lines[0].strip()
        forecast_horizon = int(lines[1].strip())
        node_names_string = lines[2].strip()  
        train_array_shape_str = lines[3].strip()
        val_array_shape_str = lines[4].strip()
        test_array_shape_str = lines[5].strip()
    
    train_array_shape = eval(train_array_shape_str)
    val_array_shape = eval(val_array_shape_str)
    test_array_shape = eval(test_array_shape_str)


    print("train_array_shape ", train_array_shape)

    with open(input_path1, 'r') as train_arr:
        read_string = train_arr.read()
        read_string=read_string.replace('[','')
        read_string=read_string.replace(']','')

        array1d=np.fromstring(read_string,dtype=float,sep=',')
        array2d=np.reshape(array1d, train_array_shape)


        train_array = array2d
    
    with open(input_path2, 'r') as val_arr:
        read_string = val_arr.read()
        read_string=read_string.replace('[','')
        read_string=read_string.replace(']','')

        array1d=np.fromstring(read_string,dtype=float,sep=',')
        array2d=np.reshape(array1d, val_array_shape)

        val_array = array2d

    with open(input_path3, 'r') as test_arr:
        read_string = test_arr.read()
        read_string=read_string.replace('[','')
        read_string=read_string.replace(']','')

        array1d=np.fromstring(read_string,dtype=float,sep=',')
        array2d=np.reshape(array1d, test_array_shape)
        
        test_array = array2d
        
    #Path(args.output_path).parent.mkdir(parents=True, exist_ok=True)
    
    # with open(output_path, 'w+') as f:
    #         lines = []
    #         lines.append(mean_string +'\n')
    #         lines.append(std_string +'\n')
    #         lines.append(node_names_string +'\n')
    #         f.writelines(lines)

    if forecast_horizon == 30:
        freq = 5
    elif forecast_horizon == 5 or forecast_horizon == 15:
        freq = 1

    #batch_size = 64
    input_sequence_length = int(forecast_horizon / freq)
    forecast_horizon = int(forecast_horizon / freq)
    multi_horizon = False

    hyperparameters_score_list = []
    node_names, adjacency_matrix = read_topology(topology_path)

    for batch_size in [64, 128]:
        for learning_rate in [0.0002, 0.005]:
            for units in [64, 128]:
                for epochs in [10, 30]:
                    
                    train_dataset, val_dataset = (
                        create_tf_dataset(metric_array, input_sequence_length, forecast_horizon, batch_size)
                        for metric_array in [train_array, val_array]
                    )

                    test_dataset = create_tf_dataset(
                        test_array,
                        input_sequence_length,
                        forecast_horizon,
                        batch_size=test_array.shape[0],
                        shuffle=False,
                        multi_horizon=multi_horizon,
                    )


                    #adjacency_matrix = np.array([np.array([1, 1, 1]), np.array( [1, 1, 0]),np.array([1, 0, 1])])
                        
                    sigma2 = 0.1
                    epsilon = 0.5
                    node_indices, neighbor_indices = np.where(adjacency_matrix == 1)
                    graph = GraphInfo(
                        edges=(node_indices.tolist(), neighbor_indices.tolist()),
                        num_nodes=adjacency_matrix.shape[0],
                    )

                    in_feat = 1
                    epochs = epochs
                    out_feat = 10
                    lstm_units = units
                    graph_conv_params = {
                        "aggregation_type": "mean",
                        "combination_type": "concat",
                        "activation": None,
                    }

                    st_gcn = LSTMGC(
                        in_feat,
                        out_feat,
                        lstm_units,
                        input_sequence_length,
                        forecast_horizon,
                        graph,
                        graph_conv_params,
                    )
                    inputs = layers.Input((input_sequence_length, graph.num_nodes, in_feat))
                    outputs = st_gcn(inputs)

                    model = keras.models.Model(inputs, outputs)
                    model.compile(
                        optimizer=keras.optimizers.RMSprop(learning_rate=learning_rate),
                        loss=keras.losses.MeanSquaredError(),
                    )
                    model.fit(
                        train_dataset,
                        validation_data=val_dataset,
                        epochs=epochs,
                        callbacks=[keras.callbacks.EarlyStopping(patience=10)],
                    )

                    y, y_pred = predict(test_dataset, test_array, model)

                    mape = evaluate_stgnn(y,y_pred)
                    hyperparameters_score_list.append((batch_size, learning_rate, units, epochs, mape))
                    print(mape)
    print(hyperparameters_score_list)
   
    best_hyperparameters = best_hyperparameters(hyperparameters_score_list)
    best_hyperparameters_str = str(best_hyperparameters)
    
    Path(args.best_hyperparameters_path).parent.mkdir(parents=True, exist_ok=True)
    with open(best_hyperparameters_path, 'w+') as f:
            hyperparameters = best_hyperparameters_str
            f.write(hyperparameters)
    # Path(args.labels_output).parent.mkdir(parents=True, exist_ok=True)
    # Path(args.preds_output).parent.mkdir(parents=True, exist_ok=True)


    # y, y_pred = predict(test_dataset, test_array, model)

    # y_string = str(y.tolist())
    # y_pred_string = str(y_pred.tolist())

    # with open(labels_output, 'w+') as f:
    #     arg_y = y_string
    #     f.write(arg_y)

    # with open(preds_output, 'w+') as f:
    #     arg_y_preds = y_pred_string
    #     f.write(arg_y_preds)


parser = argparse.ArgumentParser(description='command line arguments.')
parser.add_argument('--input_path1', type=str)
parser.add_argument('--input_path2', type=str)
parser.add_argument('--input_path3', type=str)
parser.add_argument('--input_path4', type=str)
parser.add_argument('--best_hyperparameters_path', type=str)
parser.add_argument('--topology_path', type=str)
#parser.add_argument('--output_path', type=str)
# parser.add_argument('--labels_output', type=str)
# parser.add_argument('--preds_output', type=str)


args = parser.parse_args()


if __name__ == '__main__':
	hyperparameter_tuning(args.input_path1, args.input_path2, args.input_path3, args.input_path4, args.best_hyperparameters_path, args.topology_path) #, args.output_path, args.labels_output, args.preds_output)
