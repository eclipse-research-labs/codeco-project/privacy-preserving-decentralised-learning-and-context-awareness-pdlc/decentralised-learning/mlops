<!--
  ~ Copyright (c) 2024 Intracom Telecom (ICOM)
  ~ 
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~ 
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~ 
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~ 
  ~ SPDX-License-Identifier: Apache-2.0
  ~ 
  ~ Contributors:
  ~     Maria Eleftheria Vlontzou - Author
-->

# MLOps

# Prerequisites
To use this code you should have the following:
- Python Installed
- Kubernetes: To run the code of the MLOps pipeline, it is necessary to have set up a Kubernetes cluster. 
- Kubeflow: Furthermore, Kubeflow must be installed and running. To get started with Kubeflow installation the steps described in this link: https://charmed-kubeflow.io/docs/get-started-with-charmed-kubeflow can be followed.
- KFP Compiler: In order for a pipeline to be submitted for execution, it should be compiled to YAML, using the KFP SDK compiler.
- GNN inference Service and GNN controller modules of the PDLC component deployed and running (see instructions [here](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/privacy-preserving-decentralised-learning-and-context-awareness-pdlc/PDLC-DL/pdlc-gnns/-/tree/main?ref_type=heads#instructions))
- A persistent volume and a persistent volume claim have to be created in the Kubernetes cluster in order for the Pod of the training Kubeflow component to be able to save the model in the desired Host Path of the filesystem. The deployment files for the persistent volume and persistent volume claim are provided in the Gitlab repository. 


# Description
The MLOps subcomponent of PDLC-DL is responsible for the retraining of the PDLC-DL GNN models. Within the GNN controller, a retraining module is responsible for continuously calculating the Mean Absolute Percentage Error (MAPE), in order to evaluate the models’ performance and potentially trigger a retraining procedure depending on whether the MAPE is higher than a given threshold. If retraining is necessary, the retraining module of the controller uses the KFP SDK to create a Pipeline Run in Kubeflow. This pipeline performs hyperparameter tuning and trains a new model with the optimal hyperparameters that were identified. After training, the new model is written in the shared volume, overwriting the previous one and in that way the inference service automatically uses the newly trained model.  The MLOps pipeline consists of three components, one which performs the data formatting ('/retraining_pipeline/data_formatting'), one component for hyperparameter tuning ('/retraining_pipeline/hyperparameter_tuning' and one component for training a new model with the optimal hyperparameters from the previous component and saving the new pretrained model, so that the GNN Inference service can access it ('/retraining_pipeline/training').

![Retraining](readme_pictures/retraining_workflow.png)



# How to run

- Create a storage class to immediately bind the persistent volume and the persistent volume claim:

```bash
kubectl apply -f immediate_storage_class.yaml
```

- Create a persistent volume and a persistent volume claim in the Kubernetes cluster by running:

```bash
kubectl apply -f retraining_pv.yaml 
```
```bash
kubectl apply -f retraining_pvc.yaml 
```

- Deploy the retraining submodule in the **admin** namespace where Kubeflow is running:

```bash
cd retraining
```

```bash
kubectl apply -f gnn_retraining.yaml 
```

If the retraining pipeline is triggered, you will be able to see the retraining pipeline running in the Kubeflow Dashboard:

![Retraining pipeline](readme_pictures/retraining_pipeline.png)


